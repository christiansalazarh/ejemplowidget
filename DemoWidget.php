<?php
/**
 	esqueleto de un widget funcional.
 
	puedes ver un ejemplo de uso en README.md

	en su forma mas simple, un widget puede ser asi:

		class MiWidget extends CWidget {
			public function run(){
				echo "hola";
			}
		}

	pero como te has imaginado ya eso no sirve, para eso omites el widget
	y simplemente dices: echo "hola"; y asunto resuelto..

	un widget va mas alla, su funcion es unificar el funcionamiento de 
	varias piezas de codigo:
		assets
		actions
		html
		css
	con el objeto final de proveerle al usuario final y al programador
	una herramienta -encapsulada-. (concepto de Encapsulamiento de la OOP).
	
	autor:
		Christian Salazar H. <christiansalazarh@gmail.com>
 */
class DemoWidget extends CWidget {

	// los atributos visibles del widget
	//
	public $id;					// un ID para el div mayor
	public $etiquetaNombre;		// campo de ejemplo
	public $etiquetaApellido;	// campo de ejemplo
	public $action;				// usalo para que el widget llame un action
	public $onSuccess;			// usalo para reportar situacion OK
	public $onError;			// usalo para reportar un error

	private $_baseUrl;			// usada internamente para los assets

	public function init(){
		parent::init();
		// aqui se hace cualquier inicializacion extra
	}

	public function run(){
		// este metodo sera ejecutado cuando el widget se inserta

		// preparamos los assets.
		// lo que se hace simplemente es obtener una entrada en el directorio
		// de /tuapp/assets/ (ese directorio te lo da yii con el codigo que 
		// veras dentro del metodo _prepararassets).
		// luego, copiamos a ese directorio todos nuestros scrips o css
		$this->_prepararAssets();

		// renderizamos el contenido que el widget va a darnos
		//
		// toda la funcionalidad de JAVASCRIPT, CSS ha sido
		// manejada dentro de componentes que estan en assets/
		// por tanto el widget se limita a crear la estructura
		// para usar todo eso junto y consistente.
		echo 
"
<div id={$this->id} class='demowidget'>
	<div class='row'>
		<label>{$this->etiquetaNombre}</label>
		<input type='text' name='nombre'>
	</div>
	<div class='row'>
		<label>{$this->etiquetaApellido}</label>
		<input type='text' name='apellido'>
	</div>
	<input type='button' value='OK' class='botonok'>
</div>
";

		// preparamos algunas opciones para pasarselas al
		// objeto javascript llamado DemoWidget que crearemos
		// mas abajo.
		$options = CJavaScript::encode(array(
			'action'=>CHtml::normalizeUrl($this->action), // importante
			'id'=>$this->id,
			'onSuccess'=>new CJavaScriptExpression($this->onSuccess),
			'onError'=>new CJavaScriptExpression($this->onError),
			'botonokclassname'=>'botonok',
		));

		// insertamos el objeto Javascript DemoWidget, el cual reside
		// en un archivo JS externo (en los assets).
		// le pasamos las opciones a su constructor con el objeto de 
		// comunicar las dos piezas.
		Yii::app()->getClientScript()->registerScript("demowidget_corescript"
				,"new DemoWidget({$options})");

		// nada mas es requerido.
	}// end run()

	/*	este metodo tiene como proposito desplegar los assets
		que estan en el directorio privado "maestro" del directorio
		del widget, cuyo destino sera el directorio /assets de tu app.
	*/
	public function _prepararAssets(){
		// queremos que el recurso CSS y JS que tenemos en extensions/demowidget/assets/ 
		// pase a copiarse al directorio de assets/ de la aplicacion:
		$localAssetsDir = dirname(__FILE__) . '/assets';
		// baseUrl contendrá el directorio de assets de nuestra app,
		// aparte de copiar todos los archivos alli presentes al nuevo destino.
		$this->_baseUrl = Yii::app()->getAssetManager()->publish(
				$localAssetsDir);
		// requerimos jQuery ?
		// pues solicitemos jQuery a YII, si ya estaba inserto 
		// yii no lo duplicara.
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
		// aqui le indicamos a nuestro website en su parte HEAD
		// que inserte los scripts y css que han sido publicados
		// al directorio de assets: _baseUrl
		foreach(scandir($localAssetsDir) as $f){
			$_f = strtolower($f);
			if(strstr($_f,".swp"))
				continue;
			if(strstr($_f,".js"))
				$cs->registerScriptFile($this->_baseUrl."/".$_f);
			if(strstr($_f,".css"))
				$cs->registerCssFile($this->_baseUrl."/".$_f);
		}
		// en este punto deberia haber un directorio de assets/NNNNNN
		// con una copia de todos los archivos dentro de ext/demowidget/assets/
		// y this->_baseUrl tendrá la ruta de ese directorio de assets.
	}
}
