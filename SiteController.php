<?php
class SiteController extends Controller
{
	public function actionVistaEjemplo(){
		$this->render('vistaejemplo');
	}

	// llamado por el demowidget via ajax
	public function actionDemoWidgetPost(){
		
		$datosPost = trim(file_get_contents('php://input'));
		// throw new Exception("PRUEBA DATOS: ".$datosPost);

		// este post recibe cosas como:
		//	"nombre=hola&apellido=pepe"
		// por tanto convertimos eso a algo que Yii entienda:
		$attributes = array();
		foreach(explode("&",$datosPost) as $item){
			$att = explode("=",$item);
			$attributes[$att[0]]=$att[1];
		}

		// listo, ya podriamos validar esto con un modelo:
		//
		// $model = new Persona();  // supon que tiene los atributos nombre y apellido
		// $model->attributes = $attributes;
		// if($model->validate()) { ...  }

		echo "OK. DATOS RECIBIDOS.";

		// si ocurre un error, puedes invocar a:
		// throw new Exception('Ocurrio un error x.');
		// 
		// para que no muestre el trace del error, entonces:
		// editas /index.php y pones a false los TRACE.
	}

}
