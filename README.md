Un Widget de Demostración
=========================

por: Christian Salazar	<christiansalazarh@gmail.com>

A continuación el ejemplo completo de cómo hacer un widget funcional que
presente un formulario con dos campos, que maneje el javascript (jQuery) 
necesario y que envie el resultado via ajax a un action de nuestro
controller.

El concepto principal tras un widget es EL ENCAPSULAMIENTO, es un concepto
de la OOP (Programación Orientada a Objetos) indicador de que un objeto debe
ocultar sus partes complicadas a quien lo utilice. En el caso del widget
se trata de ocultar el "cómo se hace". Al programador no le interesa ver
cómo las partes jquery, ajax, css, html se combinan para producir un 
componente de UI, solo le interesa USAR el componente de UI.

Aqui podrás conocer como conectar un widget a un Action, cómo notificar
eventos onSuccess u onError. he sido lo suficientemente corto en el código
para no opacar esta funcionalidad, minimizando el código a lo mas pequeño
posible.

Para usar este widget en cualquier parte solo debes hacer lo siguiente:

	```
	<?php
		//	un simple widget de demostracion que
		//  pide un nombre un apellido y los lanza
		//  a un action via Ajax.
		//
		$this->widget('ext.demowidget.DemoWidget'
		,array(
			'id'=>'demowidget1',
			'etiquetaNombre'=>'Nombre:',
			'etiquetaApellido'=>'Apellido:',
			'action'=>array('site/demowidgetpost'),
			'onSuccess'=>"function(respuesta){ $('#logger').html(respuesta); }",
			'onError'=>"function(texto){ $('#logger').html('error: '+texto);  }",
		));
	?>	
	<div id='logger'>..un logger..</div>
	```

lo cual proveerá el siguiente widget:

![Demo Widget](https://bitbucket.org/christiansalazarh/ejemplowidget/downloads/demowidget-screenshot.png "Demo Widget")
