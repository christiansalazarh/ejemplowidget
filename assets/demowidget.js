// el codigo JS del widget
//
var DemoWidget = function(options){

	var divMayor = $('#'+options.id);
	
	// esta funcion envia por ajax un comando POST asincrono
	// 
	var ajaxcmd = function(action,postdata,callback){
		var result=false;
		var nocache=function(){
			var dateObject = new Date();
			var uuid = dateObject.getTime();
			return "&nocache="+uuid;
		}
		jQuery.ajax({
			url: action+nocache(),
			type: 'post',
			async: false,
			contentType: "application/json",
			data: postdata,
			success: function(data, textStatus, jqXHR){
				result = data;
				if(callback != null)
					callback(true, data);
			},
			error: function(jqXHR, textStatus, errorThrown){
				callback(false, jqXHR.responseText);
				return false;
			},
		});
		return result;
	}



	// codigo que se ejecuta
	// cuando el widget inicia:
	// preparamos los controles:
	$('#'+options.id).find('input.'+options.botonokclassname).click(function(){
		
		// solo es un demo, tomamos los valores y los mandamos
		// via ajax al action dado en options, 
	
		// divMayor es definido arriba en la clase JS 
		//	representa al DIV que contiene todo
		//
		var nom = divMayor.find("[name|='nombre']").val();
		var ape = divMayor.find("[name|='apellido']").val();
		//alert(nom+' '+ape);

		// la magia de Ajax, al pasarle un array con valores,
		// este los convierte a tiras de datos que yii puede usar
		// para validar, ejemplo, el action recibirá algo como:
		//
		//	"nombre=hola&apellido=pepe"
		//	
		var datosPost = { 'nombre': nom, 'apellido': ape };

		// enviamos la cosa al action via ajax
		ajaxcmd(options.action, datosPost, function(ok, respuesta){
			if(ok == true){
				options.onSuccess(respuesta);
			}else{
				options.onError(respuesta);
			}	
		});	
	});

};
