<h1>Prueba de Widget</h1>

<p>Este widget pedira dos textos, nombre y apellido, y al hacer click en OK los enviara via ajax a un action.</p>

<?php
	//	un simple widget de demostracion que
	//  pide un nombre un apellido y los lanza
	//  a un action via Ajax.
	//
	$this->widget('ext.demowidget.DemoWidget'
	,array(
		'id'=>'demowidget1',
		'etiquetaNombre'=>'Nombre:',
		'etiquetaApellido'=>'Apellido:',
		'action'=>array('site/demowidgetpost'),
		'onSuccess'=>"function(respuesta){ $('#logger').html(respuesta); }",
		'onError'=>"function(texto){ $('#logger').html('error: '+texto);  }",
	));
?>

<div id='logger'>..un logger..</div>

